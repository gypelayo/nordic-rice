#! /bin/bash
echo "Setting up dock"

# Set dock in bottom
gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM
gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed false
gsettings set org.gnome.shell.extensions.dash-to-dock autohide true

# Change transparency
gsettings set org.gnome.shell.extensions.dash-to-dock transparency-mode 'FIXED'
gsettings set org.gnome.shell.extensions.dash-to-dock background-opacity 0.0

# Disable extended bar (Enabled centered icons)
gsettings set org.gnome.shell.extensions.dash-to-dock extend-height false

# Enable minimize apps to dock click their icons
gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize'
gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 60

echo "Setting up gnome tweeks"
sudo add-apt-repository universe

sudo apt install gnome-tweaks

mkdir ~/.themes/

echo "Setup theme"
echo "Download theme Nordic-bluish-accent.tar.xz from gnome-look and press yes"

function pause(){
   read -p "$*"
}

pause 'Press [Enter] key to continue...'

tar -xf ~/Downloads/Nordic-bluish-accent.tar.xz -C ~/.themes/
rm -r ~/Downloads/Nordic-bluish-accent.tar.xz
gsettings set org.gnome.desktop.interface gtk-theme Nordic-bluish-accent
gsettings set org.gnome.desktop.wm.preferences theme Nordic-bluish-accent

mkdir ~/.icons/

echo "Setup theme"
echo "Download icons package ppapirus-icon-theme-nordic-folders.tar.xz from gnome-look and press yes"

tar -xf ~/Downloads/papirus-icon-theme-nordic-folders.tar.xz -C ~/.icons/
gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'
rm -r ~/Downloads/papirus-icon-theme-teal-folders.tar.xz

echo "Setting up brave browser"

sudo apt install curl
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update
sudo apt install brave-browser

echo "Installing lazygit"
LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[^"]*')
curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz"
tar xf lazygit.tar.gz lazygit
sudo install lazygit /usr/local/bin

echo "Installing Visual Studio Code"
sudo snap install code --classic 

echo "Cloning background images"
git clone https://github.com/linuxdotexe/nordic-wallpapers.git ~/Pictures
echo "Setup background slideshow with Shotwell"

echo "https://www.displaylink.org/forum/showthread.php?t=68347"